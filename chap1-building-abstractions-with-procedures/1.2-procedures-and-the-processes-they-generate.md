## Procedures and the processes They Generate

### 1.2.1 Linear Recursion and Iteration

```
1 ]=> (define (factorial n)
        (if (= n 1)
            1
            (* n (factorial (- n 1)))))

;Value: factorial
```

Recursive process, with deferred operations. 
The state of the computation is hidden and maintained by interpreter, not in variables.
The longer the chain, the more information must be maintained.


```
1 ]=> (define (factorial n)
        (fact-iter 1 1 n))

;Value: factorial

1 ]=> (define (fact-iter product counter max-count)
        (if (> counter max-count)
            product
            (fact-iter (* product counter) (+ counter 1) max-count)))

;Value: fact-iter
```

Iterative process, even if contains a recursive call.
The program variables have the complete state of the computation. We can stop and resume
the computarion as long as the values in the variables (produt, counter, max-count) are hold.

Exercise 1.9
```
(define (+ a b)
  (if (= a 0) b (inc (+ (dec a) b))))

(+ 4 5)

;; substitution model:
;
;               (inc (+ 3 5))
;               (inc (inc (+ 2 5)))
;               (inc (inc (inc (+ 1 5))))
;               (inc (inc (inc (inc (+ 0 5)))))
;               (inc (inc (inc (inc 5))))
;               (inc (inc (inc 6)))
;               (inc (inc 7))
;               (inc 8)
;               9
;               
;; it's a recursive process, the interpreter (stack chain) maintains the state of computation

(define (+ a b)
  (if (= a 0) b (+ (dec a) (inc b))))

(+ 4 5)

;               (+ 3 6)
;               (+ 2 7)
;               (+ 1 8)
;               (+ 0 9)
;               9
;
;; it's an iterative process, eventhough it uses a recurvice procedure
```

1.10
Ackermann’s function
```
1 ]=> (define (A x y)
  (cond ((= y 0) 0)
        ((= x 0) (* 2 y))
        ((= y 1) 2)
        (else (A (- x 1) (A x (- y 1))))))

;Value: a

1 ]=> (A 1 10)

;Value: 1024

1 ]=> (A 2 4)

;Value: 65536

1 ]=> (A 3 3)

;Value: 65536

 (define (f n) (A 0 n))    ; ->  2*n
 (define (g n) (A 1 n))    ; ->  2^n
 (define (h n) (A 2 n))    ; ->  2^(2*n)
```


### 1.2.2 Tree Recursion

```
(define (fib n)
  (cond ((= n 0) 0)
        ((= n 1) 1)
        (else (+ (fib (- n 1)) (fib (- n 2))))))
```

For fibonacci, this is an ineficienty procedure because it calculates the same operations
multiple times. E.g, all leafs of the computation tree are either (fib 1) or (fib 0).

In general, tree recursive processes:
 - number of steps -> proportinal to number of nodes of the tree
 - space required -> proportional to max depth of tree

A more efficient Fibonacci calculation as an iterative procedure:
```
1 ]=> (define (fib-iter a b count)
        (if (= count 0)
            b
            (fib-iter (+ a b) a (- count 1))))

;Value: fib-iter

1 ]=> (define (fib n)
        (fib-iter 1 0 n))

;Value: fib

1 ]=> (fib 8)

;Value: 21
```

Exercise 1.11

f(n) = n, if n < 3
f(n) = f(n-1) + 2f(n-2) + 3f(n-3), if n >= 3

```
;;
;; recursive process
;;

(define (f n)
  (if (< n 3)
      n
      (+ (f (- n 1))
         (* 2 (f (- n 2)))
         (* 3 (f (- n 3))))))

;;
;;  iterative process
;;

(define (g n)
  (if (< n 3)
      n
      (g-iter 2 1 0 (- n 2))))

(define (g-iter a b c count)
  (if (= count 0)
      a
      (g-iter (+ a (* 2 b) (* 3 c))
              a
              b
              (- count 1))))
```


### 1.2.4  Exponentiation

```
;;
;; recursive: b^4 = b * b * b * b, O(n) space, O(n) operations
;;
(define (expt b n)
  (if (= n 0)
      1
      (* b (expt b (- n 1)))))

;;
;; iterative, b^4 = b * b * b * b, O(1) space, O(n) operations
;;
(define (expt b n)
  (expt-iter b n 1)
(define (expt-iter b counter product)
  (if (= counter 0)
      product
      (expt-iter b
                 (- counter 1)
                 (* product b))))

;;
;; fast iterative, b^16 = (((b^2)^2)^2)^2, O(log n) space, O(log n) operations
;;
(define (fast-expt b n)
  (cond ((= n 0) 1)
        ((even? n) (square (fast-expt b (/ n 2))))
        (else (* b (fast-expt b (- n 1))))))

(define (even? n)
  (= (remainder n 2) 0))
```

Exercise 1.16 - write the expt procedure as O(1) space and O(log n) operations
```
;;
;; Following the provided hint: b^n = 1 * b^n
;;
;; if n is even:
;;   1 * b^n = 1 * (2b)^(n/2) = 2 * b^(n/2)
;;
;; if n is not even:
;;   1 * b^n = 1 * (b+1)^(n-1)
;;
;; iterate until n->0, then a will be the expected output
;;
(define (iter a b n)
  (cond ((= n 0) a)
        ((even? n) (iter a (square b) (/ n 2)))
        (else (iter (* a b) b (- n 1)))))

(define (fast-expt b n)
  (iter 1 b n))
```

1.17
```
(define (double x)
  (* x 2))

(define (halve x)
  (/ x 2))

(define (mult a b)
  (cond ((= b 1) a)
        ((even? b) (double (mult a (halve b))))
        (else (+ a (mult a (- b 1))))))
```

1.18
```
(define (mult-iter a b)
  (cond ((= b 1) a)
        ((even? b) (mult-iter (double a) (halve b)))
        (else (mult-iter (+ a a) (- b 1)))))
```

1.19

T(pq) = | a = bq + aq + ap
        | b = bp + aq

Applying the transformation once:
a1 = (bp + aq)q + (bq + aq + ap)q + (bq + aq + ap)p
b1 = (bp + aq)p + (bq + aq + ap)q

Trying to find b1 = b.p' + a.q', rearranging b1 in terms of b and a:
b1 = b (p^2 + q^2) + a (2pq + q^2)

So, giving that:

  | p' = p^2 + q^2
  | q' = 2pq + q^2

a1 in T(pq) <=> a0 in T(p'q')

```
(define (p-prime p q)
  (+ (square p) (square q)))

(define (q-prime p q)
  (+ (* 2 p q) (square q)))
```


### 1.2.6 Example: Testing for Primality

N is prime if and only if N is its own smallest divisor (excluding 1).

```
(define (smallest-divisor n)
  (find-divisor n 2))

;;
;; it checks all test-divisor numbers while square(test-divisor) < n,
;;                                       or        test-divisor  < sqrt(n)
;; 
(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))

(define (divides? a b)
  (= (remainder b a) 0))

(define (prime? n)
  (= n (smallest-divisor n)))
```

Exercise 1.21
```
1 ]=> (smallest-divisor 199)

;Value: 199

1 ]=> (smallest-divisor 1999)

;Value: 1999

1 ]=> (smallest-divisor 19999)

;Value: 7
```

1.22
The prime? procedure will try to find a divisor of n between 2 and sqrt(n).
In worst case, when n is prime, it will require sqrt(n) operations: O(sqrt n)
The time results below for each prime found confirm the O(sqrt n) growth:
n = 10^12 -> .689 seconds
n = 10^13 -> 2.18 secs ~= .689 * sqrt(10)
n = 10^14 -> 6.81 secs ~= 2.18 * sqrt(10)
```
(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))))

(define (report-prime elapsed-time)
  (display " >>> ")
  (display elapsed-time))


;;
;; search in a range, from n to end, print found primes with calculation time
;;
(define (search-for-primes n end)
  (timed-prime-test n)
  (if (> n end)
      n
      (search-for-primes (+ n 2) end)))

;; n = 10^12
(search-for-primes 1000000000001 1000000000090)

; 1000000000039 >>> .6899999999999977
; 1000000000061 >>> .6800000000000068
; 1000000000063 >>> .6800000000000068

;; n = 10^13
(search-for-primes 10000000000001 10000000000100)

; 10000000000037 >>> 2.1899999999999977
; 10000000000051 >>> 2.1399999999999864
; 10000000000099 >>> 2.1500000000000057

;; n = 10^14
(search-for-primes 100000000000001 100000000000100)

; 100000000000031 >>> 6.819999999999993
; 100000000000067 >>> 6.759999999999991
; 100000000000097 >>> 6.75
```

