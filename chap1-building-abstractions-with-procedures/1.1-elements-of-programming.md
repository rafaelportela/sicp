## 1.1 The Elements of Programming

### 1.1.6 Conditional Expressions and Predicates

Normal-order evaluation: everything is expanded and evaluated
Applicative order evaluation: Only what's needed is evaluate, in order

expand vs evaluate

Exercise 1.2
```
1 ]=> (/ (+ 5 4 (- 2 (- 3 (+ 6 (/ 4 5))))) (* 3 (- 6 2) (- 2 7)))  

;Value: -37/150
```

1.3
```
;Value: sum-of-squares

1 ]=> (sum-of-squares 1 2)

;Unbound variable: sq
;To continue, call RESTART with an option number:
; (RESTART 3) => Specify a value to use instead of sq.
; (RESTART 2) => Define sq to a given value.
; (RESTART 1) => Return to read-eval-print level 1.

2 error> (RESTART 1)

;Abort!

1 ]=> (define (sq a) (* a a))

;Value: sq

1 ]=> (sum-of-squares 1 2)

: 5

1 ]=> (define (second-max a b c)
        (cond ((= a (max a b c)) (max b c))
              ((= b (max a b c)) (max a c))
              ((= c (max a b c)) (max a b))))

;Value: second-max

1 ]=> (define (sum-of-squares-of-larger-numbers a b c)
        (sum-of-square (max a b c) (second-max a b c)))

;Value: sum-of-squares-of-larger-numbers

1 ]=> (sum-of-squares-of-larger-numbers 2 2 4)

;Value: 20
```

### 1.1.7 Square roots by Newtown's method

mathematics: function, declarative, what is
computer science: procedure, imperative, how to

```
1 ]=> (define (sqrt-iter guess x ) 
        (if (good-enough? guess x)
            guess
            (sqrt-iter (improve guess x) x)))

;Value: sqrt-iter

1 ]=> (define (improve guess x)
        (average guess (/ x guess)))

;Value: improve

1 ]=> (define (average x y)
        (/ (+ x y) 2))

;Value: average

1 ]=> (define (good-enough? guess x)
        (< (abs (- (square guess) x)) 0.001))

;Value: good-enough?

1 ]=> (define (sqrt x)
        (sqrt-iter 1.0 x))

;Value: sqrt

1 ]=> (sqrt 9)

;Value: 3.00009155413138

```

Exercie 1.6

`if` is a special form. Redefining `if` as an ordinary procedure.

```
1 ]=> (define (new-if predicate then-clause else-clause)
        (cond (predicate then-clause)
                      (else else-clause)))

;Value: new-if

1 ]=> (new-if (= 2 3) 0 5)

;Value: 5

1 ]=> (define (sqrt-iter guess x)
        (new-if good-enough? guess x)
                guess
                (sqrt-iter (improve guess x) x)))

;Value: sqrt-iter

1 ]=> (sqrt 9) ; ---> this never returns
```

All parameters from new-if, the predicate, the then-clause and else-clause
are evaluated when invoking new-if. This results in the else-clause containing
a recursive call to sqrt-iter. As opposed to the regular if, where at some
point, when good-enough? return true, the then-clase (plain guess) would be
returned and the else-clause would not be invoked.

