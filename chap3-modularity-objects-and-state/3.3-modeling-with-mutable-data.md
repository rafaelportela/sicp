## 3.3 Modeling with Mutable Data

Data abstractions will include, in addition to selectors and constructors,
operations called _mutators_, which modify data objects.

Data objects from which mutators are defined are known as _mutable data objects_.

### 3.3.1 Mutable List Structure

Exercise 3.12
```
(define x (list 'a 'b))
(define y (list 'c 'd))
(define z (append x y))

(cdr x)
; (b)

(define w (append! x y))

(cdr x)
; (b c d)
```

3.13

Given make-cycle links the end of the list to the first element, creates a cycle,
last-pair will not finish its execution when called with such ciclic list.


*Sharing and identity*

In general sharing is completely undetectable if we operate on lists using only
cons, car and cdr. However, if we allow mutators on list structure, sharing becomes
significant. Sharing can be dangerous since modifications made to structures will
also affect other structures that happen to share the modified parts.


*Mutation is just assignment*


### 3.3.2 Representing Queues

```
(define (front-ptr queue) (car queue))
(define (rear-ptr queue) (cdr queue))
(define (set-front-ptr! queue item)
  (set-car! queue item))
(define (set-rear-ptr! queue item)
  (set-cdr! queue item))

(define (empty-queue? queue)
  (null? (front-ptr queue)))

(define (make-queue) (cons '() '()))

(define (front-queue queue)
  (if (empty-queue? queue)
      (error "FRONT called with an empty queue" queue)
      (car (front-ptr queue))))

(define (insert-queue! queue item)
  (let ((new-pair (cons item '())))
    (cond ((empty-queue? queue)
           (set-front-ptr! queue new-pair)
           (set-rear-ptr! queue new-pair)
           queue)
          (else
           (set-cdr! (rear-ptr queue) new-pair)
           (set-rear-ptr! queue new-pair)
           queue))))

(define (delete-queue! queue)
  (cond ((empty-queue? queue)
         (error "DELETE! called with an empty queue" queue))
        (else (set-front-ptr! queue (cdr (front-ptr queue)))
              queue)))
```

Exercise 3.21
```
(define (print-queue queue)
  (define (print-iter q)
    (cond ((null? q) ())
          (else
            (display " > ")
            (display (car q))
            (print-iter (cdr q)))))
  (print-iter (front-ptr queue)))
```


### 3.3.3 Representing Tables

```
(define (lookup key table)
  (let ((record (assoc key (cdr table))))
    (if record
        (cdr record)
        false)))

(define (assoc key records)
  (cond ((null? records) false)
        ((equal? key (caar records)) (car records))
        (else (assoc key (cdr records)))))

(define (insert! key value table)
  (let ((record (assoc key (cdr table))))
    (if record
        (set-cdr! record value)
        (set-cdr! table
                  (cons (cons key value)
                        (cdr table)))))
  'ok)

(define (make-table)
  (list '*table*))
```
