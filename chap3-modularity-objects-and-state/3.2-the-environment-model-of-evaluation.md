## 3.2. The Environment Model of Evaluation

An environment is a sequence of frames. Each fram is a table (possibly emptyf) of
bindings, which associate variables names with their corresponding values (bindings).

Each frame also has a pointer to its enclosing environment, unless the frame is
global.


### 3.2.1 The Rules for Evaluation

Evaluation model (using environment frames) vs substitution model.


### 3.2.2 Applying Simple Procedures


### 3.2.3 Frames as the Repository of Local State

Calling:
```
(define W1 (make-withdraw 100))
```
It creates a new environment structure.
In the global env, W1 is created and points to a pair with some code and a new
environment. The new env contains a frame with its own local bindings for the
balance variable (balance: 100).


### 3.2.4 Internal Definitions

Internal definitions, like a new procedure definition confined within a procedure,
will have their own environments at runtime to hold their local state.
